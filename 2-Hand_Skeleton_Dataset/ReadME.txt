the dataset is divided into two folders:

- one containing the data used for network training
- one containing the data used for the network test

the files inside the folders are organized as follows:

- each file represents a feature
- each line of a file represents an instance, whose class is indicated in the file y_train.txt (or y_test in the Test folder) at the corresponding line
- each value in the row represents the value acquired in a frame
- each value is separated by a blank space

The index of the class are associated based on the following gesture order: 1, 2-V, 3, 4, 5, 6-W, 7, 8, 9, A, B, C, D, H, I, L, X, Y, finish, green, hungry, milk, past, pig, store, and where.